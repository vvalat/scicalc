require "minitest/autorun"
require "scilab"

class OpTest < Minitest::Test
  def test_op
    assert_equal "11",
                 Operations.add(5, 6)
  end
end
