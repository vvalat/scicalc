require_relative "scicalc/version"
require_relative "scicalc/base"
require_relative "scicalc/memory"
require_relative "scicalc/operations"

#module Scicalc
	def select_op
  		puts "\n"
  		Base.base_display
  		puts "You can switch base system by inputing 'switch'\n \n"
  		puts "You can convert any number by inputing 'convert'\n \n"
  		puts "Available operations for x and y:\n \n Single argument:\n Trigonometry: acos x    asin x    atan x    cos x    sin x    tan x\n Math: x^2    x^3    sqrt x    cbrt x    log x    ln x    abs x    x!\n \n Double arguments :\n Math: x + y    x - y    x * y    x / y    x^y\n \n Type your operation by inputing directly the value or calling the value in memory with m1 or m2\n"
  		Memory.memory_display
  		calc = gets
  		if (call_op(calc) == false)
    		select_op()
  		else
    		result = call_op(calc)
    		puts "Result is " + result.to_s + ". Do you wish to store this value in memory? [*y*/n]"
    		answer = gets
    		if (answer != "n\n")
      			Memory.memory_fill(result, 10)
    		end
    		select_op()
  		end
	end
	def call_op(string)
  		args = string.split(" ")
  		case
  		when args.count == 1
    		case
    		when args[0].match(/^\d+\^2$/)
      			return Operations.exp2(args[0].sub(/\^2$/, ''))
	    	when args[0].match(/^\d+\^3$/)
	      		return Operations.exp3(args[0].sub(/\^3$/, ''))
	    	when args[0].match(/^\d+\!$/)
	      		return Operations.fact(args[0].sub(/\!$/, ''))
	    	when args[0].match(/^\d+\^\d+$/)
	      		return Operations.exp(args[0].split('^'))
	    	when args[0] == "convert"
	      		result = Base.base_convert
	      		puts "Result is " + result[0].to_s + ". Do you wish to store this value in memory? [*y*/n]"
	      		answer = gets
	      		if (answer != "n\n")
	        		Memory.memory_fill(result[0], result[1])
	      		end
	      		select_op()
	    	when args[0] == "switch"
	      		Base.base_switch
	      		select_op()
	    	else
	      		puts "This does not look like something I can compute\n"
	      		return false
	    	end
	  	when args.count == 2
	    	case
	    	when args[0] == "acos"
	    		return Operations.my_acos(args[1])
	    	when args[0] == "asin"
	    		return Operations.my_asin(args[1])
    		when args[0] == "atan"
    			return Operations.my_atan(args[1])
    		when args[0] == "cos"
    			return Operations.my_cos(args[1])
    		when args[0] == "sin"
    			return Operations.my_sin(args[1])
    		when args[0] == "tan"
    			return Operations.my_tan(args[1])
   			when args[0] == "sqrt"
    			return Operations.my_sqrt(args[1])
    		when args[0] == "cbrt"
    			return Operations.my_cbrt(args[1])
    		when args[0] == "log"
    			return Operations.my_log(args[1])
    		when args[0] == "ln"
    			return Operations.my_ln(args[1])
    		when args[0] == "abs"
    			return Operations.abs(args[1])
    		else
      			puts "This does not look like something I can compute\n"
      			return false
    		end
  		when args.count == 3
    		case
    		when args[1] == "+"
    			return Operations.add(args[0], args[2])
    		when args[1] == "-"
    			return Operations.sub(args[0], args[2])
    		when args[1] == "*"
    			return Operations.prod(args[0], args[2])
    		when args[1] == "/"
    			return Operations.div(args[0], args[2])
    		else
      			puts "This does not look like something I can compute\n"
      			return false
    		end
  		else
	      	puts "This does not look like something I can compute\n"
	      	return false
	    end
	end
#end