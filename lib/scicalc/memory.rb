class Memory
  attr_accessor :mem1, :mem2
  @@mem1 = ["", ""]
  @@mem2 = ["", ""]
  def self.memory_erase
    @@mem1 = ["", ""]
    @@mem2 = ["", ""]
  end
  def self.memory_display
    if (@@mem1[1] == "")
      separator1 = ""
    else
      separator1 = ", base "
    end
    if (@@mem2[1] == "")
      separator2 = ""
    else
      separator2 = ", base "
    end
    puts "1[" + @@mem1[0].to_s + separator1 + @@mem1[1].to_s + "] 2[" + @@mem2[0].to_s + separator2 + @@mem2[1].to_s + "]\n"
  end
  def self.memory_fill(val, base)
    self.memory_display()
    puts "Choose a slot to fill (type 1 or 2):"
    slot = gets.to_i
    case
    when slot == 1
      @@mem1 = [val.to_s, base]
      return true
    when slot == 2
      @@mem2 = [val.to_s, base]
      return true
    else
      puts "There is no such slot"
      self.memory_fill(val, base)
    end
  end
end