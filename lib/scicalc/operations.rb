class Operations

  def self.add(x, y)
    return x.to_i + y.to_i
  end
  def self.sub(x, y)
    return x.to_i - y.to_i
  end
  def self.prod(x, y)
    return x.to_i * y.to_i
  end
  def self.div(x, y)
    return x.to_i / y.to_i
  end
  def self.exp2(x)
    return x.to_i ** 2
  end
  def self.exp3(x)
    return x.to_i ** 3
  end
  def self.exp(array)
    return array[0].to_i ** array[1].to_i
  end
  def self.fact(x)
    if (x.to_i <= 1)
      return 1
    else
      return x.to_i * fact( x.to_i - 1 )
    end
  end
  def self.my_acos(x)
    return Math::acos(x.to_i * Math::PI / 180 )
  end
  def self.my_asin(x)
    return Math::asin(x.to_i * Math::PI / 180 )
  end
  def self.my_atan(x)
    return Math::atan(x.to_i * Math::PI / 180 )
  end
  def self.my_cos(x)
    return Math::cos(x.to_i * Math::PI / 180 )
  end
  def self.my_sin(x)
    return Math::sin(x.to_i * Math::PI / 180 )
  end
  def self.my_tan(x)
    return Math::tan(x.to_i * Math::PI / 180 )
  end
  def self.my_sqrt(x)
    return Math::sqrt(x.to_i)
  end
  def self.my_cbrt(x)
    return Math::cbrt(x.to_i)
  end
    def self.my_log(x)
    return Math::log10(x.to_i)
  end
  def self.my_ln(x)
    return Math::log(x.to_i)
  end
  def self.abs(x)
    if (x.to_i < 0)
      return -x.to_i
    else
      return x.to_i
    end
  end
end