class Base
  attr_accessor :current_base, :base_name
  @current_base = 10 # available : decimal(10), octal(8), binary(2), hexadecimal(16)
  @base_name = "decimal"
  def self.base_display
    puts "You are currently using " + @base_name + " system."
  end
  def self.base_switch
    puts "Switch to:\n 1. Decimal (default)\n 2. Hexadecimal\n 3. Binary\n 4. Octal\n"
    base_select = gets.to_i
    case
    when base_select == 2
      @current_base = 16
      @base_name = "hexadecimal"
    when base_select == 3
      @current_base = 2
      @base_name = "binary"
    when base_select == 4
      @current_base = 8
      @base_name = "octal"
    else
      @current_base = 10
      @base_name = "decimal"
    end
    return true
  end   
  def self.base_convert
    puts "The current system is " + @base_name + ". You can convert to:\n 1. Decimal (default)\n 2. Hexadecimal\n 3. Binary\n 4. Octal\n"
    convert_select = gets.to_i
    case
    when convert_select == 1
      to = 10
    when convert_select == 2
      to = 16
    when convert_select == 3
      to = 2
    else
      to = 10
    end
    puts "What number do you wish to convert?"
    nb = gets
    if self.check_number(nb) == false
      puts "This number does not match " + @base_name + " system."
      return nil
    end
    if (@current_base != 10) # We convert to decimal
      nb = nb.chomp
      nb = nb.to_i(@current_base)
    end  
    return [nb.to_s(to), to]
  end
  def self.check_number(nb)
    nb = nb.to_s
    case
    when @current_base == 10
      if !nb.match(/^\d+$/)
        return false
      end
      return true
    when @current_base == 16
      if !nb.match(/^\h+$/)
        return false
      end
      return true
    when @current_base == 8
      if !nb.match(/^[0-7]+$/)
        return false
      end
      return true
    when @current_base == 2
      if !nb.match(/^[0-1]+$/)
        return false
      end
      return true
    end
  end
end